import pandas as pd
from datetime import datetime
import os
import pickle
from functools import reduce, partial

from pyspark.sql import functions as F
from pyspark.sql.functions import col, date_format
from pyspark.sql import SQLContext, DataFrame

class FeatureStoreUtils:

    def __init__(self,sql_context, baseStoragePath = r"/home/filipebisoffi/Documents/Projeto-feature-store/FeatureStoreMVP"):
        ''' Constructor for this class. '''
        self.baseStoragePath = baseStoragePath
        self.sql_context = sql_context
        self.availableFeatures = self.getFeatures()

    def saveToFeatureStore(self,dataSet,groupingKey,featuresToSave):

        try:
            dataSet[featuresToSave + groupingKey + ['snapshotDate']]
        except:
            raise ValueError('Error while selecting dataSet columns, check if snapshotDate, grouping key and features to save exist.')

        self.logDataSet(dataSet.select(featuresToSave + groupingKey + ['snapshotDate']))
        dataSet = dataSet.withColumn("snapshotDate",date_format(col("snapshotDate"), "yyy-MM-dd"))

        dataFrameList = list(map(lambda x: {'feature':x , 'dataFrame': dataSet.select(groupingKey+['snapshotDate'] + [x])}\
                                 , featuresToSave ))
        for i in dataFrameList:
            currentTime= datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            properties = {i['feature']: {'groupingKey': groupingKey, \
                                         'lastUpdate': currentTime, \
                                         'schema': i['dataFrame'].schema}}
            pathToSave = self.baseStoragePath + "/features/{}/".format(i['feature'])
            i['dataFrame'].write.mode('overwrite').partitionBy("snapshotDate").parquet(pathToSave) 
            with open(pathToSave + 'properties.pkl', 'wb') as fileToDump:
                 pickle.dump(properties, fileToDump)
        
        
    def getFeatures(self):
        try:
            os.listdir(self.baseStoragePath + '/features')
        except:
            return([])
        return(os.listdir(self.baseStoragePath + '/features'))
    
    def getFirstDate(self,feature):
        baseStoragePath = r"/home/filipebisoffi/Documents/Projeto-feature-store/FeatureStoreMVP"
        featurePath = baseStoragePath + "/features/{}/".format('vl_transferencia') 
        storedDates = list(filter(lambda k: 'snapshotDate=' in k, sorted(os.listdir(featurePath)) ))
        firstDate = storedDates[0].split("=")[1]
        return(firstDate)


    def getLatestDate(self,feature):
        baseStoragePath = r"/home/filipebisoffi/Documents/Projeto-feature-store/FeatureStoreMVP"
        featurePath = baseStoragePath + "/features/{}/".format('vl_transferencia') 
        storedDates = list(filter(lambda k: 'snapshotDate=' in k, sorted(os.listdir(featurePath)) ))
        lastDate = storedDates[-1].split("=")[1]
        return(lastDate)

    def logDataSet(self,dataSet):
        now = datetime.now()
        currentDate = now.strftime("%Y%m%d")
        currentTime = now.strftime("%H%M%S")
        pathToLog = self.baseStoragePath + "/logs/processedDataFrames/{}/{}".format(currentDate,currentTime)
        dataSet.write.mode('overwrite').parquet(pathToLog)
    
    def readFeatureByRange(self,feature,startDate = None,endDate = None):
        '''Use YYYY-MM-DD format to date parameters'''
        if startDate == None:
            startDate = self.getFirstDate(feature)
        if endDate == None:
            endDate = self.getLatestDate(feature)

        dateRangeToRead = pd.date_range(startDate, endDate).tolist()
        featureDf = self.readFeatureByList(feature,dateRangeToRead)
        return(featureDf)
    
    def readFeatureByList(self,feature,dateList):
        if not feature in self.availableFeatures:
            print("Warning! There is no feature '{}' in the provided path".format(feature))
            return(None)
        featurePath = self.baseStoragePath + "/features/{}/snapshotDate=".format(feature) 
        datesToRead = [featurePath + str(i)[:10] for i in dateList]
        
        existingDatesToRead = list(filter(os.path.exists,datesToRead))
        if len(existingDatesToRead) == 0:
            print("Warning! Feature: {} has no data for the requested dates".format(feature))
            return(None)
        featureDf = self.sql_context.read.option("basePath", self.baseStoragePath).parquet(*existingDatesToRead)
        return(featureDf)        
        
    def readMultipleByRangeFeatures(self,readerParameters, joinKeys, joinMethod = 'outer'):
        individualFeatureDF = list(map(lambda x: self.readFeatureByRange(x['feature'],x['startDate'],x['endDate']), readerParameters ))
        validIndividualFeatureDF =  [df for df in individualFeatureDF if df is not None]
        if len(validIndividualFeatureDF) == 0:
            print("Warning! No valid dataframes in output, please review parameters.")
            return(None)
        combinedFeatureDF = reduce(partial(DataFrame.join, on = joinKeys, how = joinMethod), validIndividualFeatureDF)
        return(combinedFeatureDF)